package se.hc.database.example;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class JdbcExampleTest {

    JdbcExample example;

    @Before
    public void setup() {

        example = new JdbcExample();
    }

    @Test
    public void testAll() throws Exception {

        try {
            example.dropTable();
        }
        catch (Exception e) {
            System.out
                    .println("Exception when dropping table. Mayble table doesn´t exist yet.");
        }
        example.createTable();

        long id = example.createUser("Bertil");
        assertTrue("ID is set", id > 0);

        User user = example.getUser(id);
        assertEquals("Name is set after create", "Bertil", user.getName());

        user.setName("Kalle");
        example.updateUser(user);
        user = example.getUser(user.getId());
        assertEquals("Name is updated", "Kalle", user.getName());

    }

}
