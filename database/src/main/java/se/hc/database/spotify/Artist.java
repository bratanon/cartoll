package se.hc.database.spotify;

import java.util.HashSet;
import java.util.Set;

public class Artist {
    private long id;
    private String name;
    private Set<Album> albums = new HashSet<Album>();

    public Artist (long id, String name) {
        setId(id);
        setName(name);
    }

    public Artist (String name) {
        setName(name);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(Set<Album> albums) {
        this.albums = albums;
    }

    @Override
    public String toString() {
        return "Artist [id=" + id + ", name=" + name + ", albums=" + albums
                + "]";
    }
}
