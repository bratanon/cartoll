package se.hc.database.spotify;

public class Song {
	private long id;
	private String name;
	private int lengthInSeconds;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLengthInSeconds() {
		return lengthInSeconds;
	}

	public void setLengthInSeconds(int length) {
		this.lengthInSeconds = length;
	}

}
