package se.hc.database.spotify.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import se.hc.database.spotify.Album;
import se.hc.database.spotify.Artist;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

/**
 * A repository for Spotify, storing artists and albums This class is tested by
 * SpotifyDaoTest. Run the test and implements all the needed logic.
 */
public class SpotifyDao {
    Logger log = Logger.getLogger(SpotifyDao.class);

    /**
     * Returns a Connection to a database.
     */
    protected Connection getConnection() throws InstantiationException,
            IllegalAccessException, ClassNotFoundException, SQLException {
        MysqlDataSource ds = new MysqlDataSource();
        ds.setServerName("localhost");
        ds.setDatabaseName("test");
        ds.setUser("root");
        ds.setPassword("password");
        return ds.getConnection();
    }

    public void init(Set<Artist> artists) throws Exception {
        // Clear all tables
        dropTable("albums");
        dropTable("artists");

        // Create tables
        executeSql("CREATE TABLE `artists` (`id` INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, `name` VARCHAR(50) NOT NULL, UNIQUE(`name`))");
        executeSql("CREATE TABLE `albums` (`id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(45) NOT NULL , `release_year` INT NOT NULL , `artist_id` INT NOT NULL , PRIMARY KEY (`id`) , CONSTRAINT FOREIGN KEY (`artist_id`) REFERENCES `artists` (`id`) ON DELETE CASCADE, UNIQUE(`name`));");

        // Insert all artists into the database. First, only insert artists.
        // Later we will store albums for each artist.
        for (Artist artist : artists) {
            createArtist(artist.getId(), artist);

            for (Album album : artist.getAlbums()) {
                createAlbum(album);
            }
        }

    }

    public Artist getArtist(long artistId) throws Exception {
        log.debug("Get artists with id " + artistId);

        ResultSetHandler resultSetHandler = new ResultSetHandler() {
            @Override
            public Object handleResultSet(ResultSet rs) throws Exception {
                Artist artist = null;
                while (rs.next()) {
                    artist = new Artist(rs.getInt("id"), rs.getString("name"));
                }
                return artist;
            }
        };

        String queryt = "SELECT ar.id, ar.name FROM artists ar WHERE ar.id = "
                + artistId;

        query(queryt, resultSetHandler);

        Artist artist = (Artist) resultSetHandler.getResult();

        if (artist != null) {
            Set<Album> albums = getAlbumsForArtist(artist);
            artist.setAlbums(albums);
        }
        return artist;
    }

    private void query(String queryt, ResultSetHandler resultSetHandler) {
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            con = getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(queryt);
            resultSetHandler.handleResultSetInternal(rs);
        }
        catch (Exception e) {
            log.debug(e.getMessage());
        }
        finally {
            try {
                stmt.close();
                con.close();
            }
            catch (Exception e) {
                log.debug(e.getMessage());
            }
        }
    }

    public Set<Album> getAlbumsForArtist(Artist artist) throws Exception {
        // Implement ResultSetHandler

        ResultSetHandler resultSetHandler = new ResultSetHandler() {

            @Override
            public Object handleResultSet(ResultSet rs) throws Exception {
                HashSet<Album> albums = new HashSet<Album>();

                while (rs.next()) {
                    albums.add(new Album(rs.getInt("id"), rs.getString("name"),
                            rs.getInt("release_year")));
                }

                return albums;
            }
        };

        // Query
        String query = "SELECT * FROM albums WHERE artist_id = "
                + artist.getId();

        query(query, resultSetHandler);

        Set<Album> albums = (Set<Album>) resultSetHandler.getResult();

        return albums;
    }

    public void updateArtist(Artist artist) throws Exception {
        String query = "UPDATE artists SET `name` = '" + artist.getName()
                + "' WHERE id = " + artist.getId();
        executeSql(query);
    }

    public void deleteArtist(long artistId) throws Exception {
        String query = "DELETE FROM artists WHERE id = " + artistId;
        executeSql(query);
    }

    public int getArtistsSize() throws Exception {
        String query = "SELECT COUNT(*) AS noArtists FROM artists";

        ResultSetHandler resultSetHandler = new ResultSetHandler() {
            @Override
            public Object handleResultSet(ResultSet rs) throws Exception {
                Integer size = 0;
                while (rs.next()) {
                    size = rs.getInt("noArtists");
                }
                return size;
            }
        };

        query(query, resultSetHandler);

        return (Integer) resultSetHandler.getResult();
    }

    /**
     * Creates an artist that has no ID. ID should be set automatically
     */
    public long createArtist(Artist artist) throws Exception {
        String query = String.format(
                "INSERT INTO artists (`name`) VALUES('%s')",
                artist.getName());
        long lastId = executeSql(query);
        return lastId;
    }

    public long createArtist(Long id, Artist artist) throws Exception {
        String query = "INSERT INTO artists (id, `name`) VALUES(" + id + " , '"
                + artist.getName() + "');";
        long lastId = executeSql(query);
        return lastId;
    }

    public long createAlbum(Album album) throws Exception {
        String query = "INSERT INTO albums (`name`, release_year, artist_id) VALUES('"
                + album.getName()
                + "', '"
                + album.getReleaseYear()
                + "', '"
                + album.getArtist().getId() + "');";
        long lastId = executeSql(query);
        return lastId;
    }

    /**
     * Probably not a typical method, but represents the concept of updating
     * multiple values in one transaction. If any update fails the whole
     * transaction should be aborted.
     * 
     * Try using a PreparedStatement instead of a normal Statement
     * 
     */
    public void updateArtistNameAndAlbumName(Artist artist,
            String newArtistName, Album album, String newAlbumName)
            throws Exception {

        Connection connection = getConnection();
        String artistQuery = "UPDATE artists SET `name` = ? WHERE id = ?";
        String albumQuery = "UPDATE albums SET `name` = ? WHERE id = ?";
        PreparedStatement artistStmt = connection.prepareStatement(artistQuery);
        PreparedStatement albumStmt = connection.prepareStatement(albumQuery);
        connection.setAutoCommit(false);

        try {
            artistStmt.setString(1, newArtistName);
            artistStmt.setLong(2, artist.getId());
            artistStmt.executeUpdate();

            albumStmt.setString(1, newAlbumName);
            albumStmt.setLong(2, album.getId());
            albumStmt.executeUpdate();
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
            throw e;
        }
        finally {
            artistStmt.close();
            albumStmt.close();
            connection.close();
        }

    }

    protected long executeSql(String query) {
        log.debug("Query: " + query);
        Connection connection = null;
        Statement stmt = null;

        long lastId = 0;
        try {
            connection = getConnection();
            stmt = connection.createStatement();
            stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);

            ResultSet generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                lastId = generatedKeys.getLong(1);
            }

            log.debug("lastID : " + lastId);
        }
        catch (Exception e) {
            log.debug(e.getMessage());
        }
        finally {
            try {
                stmt.close();
                connection.close();
            }
            catch (SQLException e) {
            }
        }
        return lastId;
    }

    protected void dropTable(String tableName) {
        log.debug("Dropping table: " + tableName);

        try {
            executeSql("DROP TABLE IF EXISTS " + tableName);
        }
        catch (Exception e) {
            log.debug("Couldn't drop table " + tableName + ".");
            log.debug(e.getMessage());
        }
    }

    /**
     * Class that handles the logic of parsing the ResultSet. Different usecases
     * will use a different parsing logic.
     * 
     */
    protected abstract class ResultSetHandler {

        private Object result;

        /**
         * Called by the DAO. Simply stores the result in a variable for later
         * retrieval.
         */
        public void handleResultSetInternal(ResultSet rs) throws Exception {
            this.result = handleResultSet(rs);
        }

        /**
         * The method that must be implemented by each usecase.
         */
        public abstract Object handleResultSet(ResultSet rs) throws Exception;

        public Object getResult() {
            return result;
        }
    }
}
