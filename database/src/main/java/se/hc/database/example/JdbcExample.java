package se.hc.database.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class JdbcExample {
    Logger log = Logger.getLogger(JdbcExample.class);

    /**
     * Returns a Connection to a database. Can be done in different ways. This
     * methods shows you different ways of getting a Connection
     */
    protected Connection getConnection() throws InstantiationException,
            IllegalAccessException, ClassNotFoundException, SQLException {

        String username = "root";
        String password = "password";
        String servername = "localhost";
        String schemaName = "test";
        int port = 3306;
        String connectionUrl = "jdbc:mysql://" + servername + ":" + port + "/"
                + schemaName;
        Connection conn = null;

        int connectVersion = 3;

        switch (connectVersion) {

            case 1:
                // Option 1 - older version
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(connectionUrl, username,
                        password);
                break;

            case 2:
                // Option 2 - A bit more modern
                Properties connectionProps = new Properties();
                connectionProps.put("user", username);
                connectionProps.put("password", password);
                conn = DriverManager.getConnection(connectionUrl,
                        connectionProps);
                break;

            case 3:
                // A DataSource is normally the recommended way to get a
                // Connection.
                // But the DataSource
                // should be created by the application server and not manually.
                // This is more common
                // in larger projects.
                MysqlDataSource ds = new MysqlDataSource();
                ds.setServerName(servername);
                ds.setDatabaseName(schemaName);
                ds.setUser(username);
                ds.setPassword(password);
                conn = ds.getConnection();
                break;

            default:

                throw new RuntimeException("No match for switch statement");
        }

        return conn;

    }

    public void dropTable() throws Exception {

        Connection con = getConnection();
        Statement stmt = con.createStatement();
        try {
            stmt.executeUpdate("DROP TABLE users");
        }
        finally {
            stmt.close();
            con.close();
        }
    }

    public void createTable() throws Exception {

        String sql = "create table users (ID INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, NAME VARCHAR(30))";

        Connection con = getConnection();
        Statement stmt = con.createStatement();
        try {
            stmt.executeUpdate(sql);
        }
        finally {
            stmt.close();
            con.close();
        }
    }

    public long createUser(String name) throws Exception {
        String sql = "Insert into users (name) values ('" + name + "')";

        Connection con = getConnection();
        Statement stmt = con.createStatement();
        try {
            stmt.executeUpdate(sql);
        }
        finally {
            stmt.close();
            con.close();
        }

        return getMaxUserId();

    }

    public User getUser(long id) throws Exception {

        User user = new User();
        Connection con = getConnection();
        Statement stmt = con.createStatement();
        ResultSet rs = null;
        try {
            rs = stmt
                    .executeQuery("select id, name from users where id =" + id);
            while (rs.next()) {
                user.setId(rs.getInt("id"));
                user.setName(rs.getString("name"));
            }

        }
        finally {
            rs.close();
            stmt.close();
            con.close();
        }

        return user;
    }

    /**
     * This method uses a transaction, and PreparedStatement
     */
    public void updateUser(User user) throws Exception {

        Connection conn = getConnection();
        PreparedStatement preparedStmt = conn
                .prepareStatement("UPDATE users SET name = ? WHERE ID = ?");
        conn.setAutoCommit(false);

        try {
            preparedStmt.setString(1, user.getName());
            preparedStmt.setLong(2, user.getId());

            log.debug(preparedStmt.toString());
            preparedStmt.executeUpdate();
            conn.commit();

        }
        catch (SQLException e) {
            if (conn != null) {
                System.err
                        .println("Transaction is being rolled back: Exception: "
                                + e.getMessage());
                conn.rollback();
            }
            throw e;
        }
        finally {
            preparedStmt.close();
            preparedStmt.close();
            conn.close();
        }
    }

    /**
     * Gets the highest ID for the users table
     */
    protected long getMaxUserId() throws Exception {

        long id = 0;

        Connection con = getConnection();
        Statement stmt = con.createStatement();
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select max(id) from users");
            while (rs.next()) {
                id = rs.getInt(1);
            }

        }
        finally {
            rs.close();
            stmt.close();
            con.close();
        }

        return id;

    }

}
