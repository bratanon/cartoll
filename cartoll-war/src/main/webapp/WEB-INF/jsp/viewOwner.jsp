<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:url value="/" var="indexUrl" />
<!DOCTYPE html>
<html>
<head>
<%@ include file="head.jsp"%>
<title>${owner.fullName}- Passagejakt</title>
</head>
<body>
    <%@ include file="header.jsp"%>

    <div id="allContent">
        <div class="backButton">
            <a href="${indexUrl}">Tillbaka till startsidan</a>
        </div>

        <h1>${owner.fullName}</h1>
        <div class="pageDescription">
            <p>${owner.address}</p>
            <p>${owner.zipCode}${owner.city}</p>

            <div class="claerfix">
                <table>
                    <thead>
                        <tr>
                            <th>Reg No</th>
                            <th>Fordonstyp</th>
                            <th>Antal passager</th>
                            <th>Kostand</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${itemWrapper}" var="item">
                            <tr>
                                <td>${item.vehicle.regNo}</td>
                                <td>${item.vehicle['class'].simpleName}</td>
                                <td>${item.statistic.passageQuantity}
                                    st</td>
                                <td>${item.statistic.income}kr</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>

        </div>
    </div>

    <%@ include file="footer.jsp"%>

</body>
</html>