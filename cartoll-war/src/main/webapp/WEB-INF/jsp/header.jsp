<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<spring:url value="/" var="indexUrl" />
<spring:url value="/addPassage.html" var="addPassage" />
<spring:url value="/clearAllPassages.html" var="clearAllPassages" />

<div id="header">
  <div id="header-inner">
    <a href="${indexUrl}" id="logo"><img alt="Passagejakt.se" src="<%=request.getContextPath() %>/theme/images/logo_passagejakt.png"></a>
    <div id="headerTabs">
      <ul>
        <li class="active"><a href="${indexUrl}">Startsidan</a></li>
        <li><a href="${addPassage}">Lägg till passage</a></li>
        <li><a href="${clearAllPassages}">Rensa passager</a></li>
      </ul>
    </div>
    <div class="headerImages"></div>
  </div>
</div>