<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:url value="/" var="indexUrl" />
<!DOCTYPE html>
<html>
<head>
<%@ include file="head.jsp"%>
<title>${station.name}- Passagejakt</title>
</head>
<body>
    <%@ include file="header.jsp"%>

    <div id="allContent">

        <div class="backButton">
            <a href="${indexUrl}">Tillbaka till startsidan</a>
        </div>

        <h1>${station.name}</h1>
        <div class="pageDescription">
            <p>Utmärkt val av station.</p>
            <p>${station.name}passar båda sommar som vinter, till
                gammal som ung, med taxi, lastbil eller vanlig bil.</p>

            <div class="clearfix">
                <table>
                    <thead>
                        <tr>
                            <th>Reg No</th>
                            <th>Fordonstyp</th>
                            <th>Ägare</th>
                            <th>Intäkt</th>
                            <th>Datum</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${passages}" var="passage">
                            <spring:url
                                value="/owner/${passage.vehicle.owner.id}.html"
                                var="ownerUrl" />
                            <tr>
                                <td>${passage.vehicle.regNo}</td>
                                <td>${passage.vehicle['class'].simpleName}</td>
                                <td><a href="${ownerUrl}">${passage.vehicle.owner.fullName},
                                        ${passage.vehicle.owner.city}</a></td>
                                <td>${passage.cost} kr</td>
                                <td>${passage.formatedDateTime}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>

        </div>
    </div>

    <%@ include file="footer.jsp"%>

</body>
</html>
