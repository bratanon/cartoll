<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <%@ include file="head.jsp"%>
        <title>Passering tillagd - Passagejakt</title>
    </head>
<body>
    <%@ include file="header.jsp"%>

    <div id="allContent">
        <div class="backButton">
            <a href="${indexUrl}">Tillbaka till startsidan</a>
        </div>
        
        <h1>Passering tillagd</h1>
        <div class="pageDescription">
            <table>
                <tr>
                    <th>ID</th>
                    <td>${passage.id}</td>
                 </tr>
                 <tr>
                    <th>Fordon</th>
                    <td>${passage.vehicle.vehicleInformationString}</td>
                 </tr>
                 <tr>
                    <th>Station</th>
                    <td>${passage.station.name}</td>
                 </tr>
                 <tr>
                    <th>Tid</th>
                    <td>${passage.formatedDateTime}</td>
                 </tr>
                 <tr>
                    <th>Kostand</th>
                    <td>${passage.cost}</td>
                 </tr>
             </table>

        </div>
    </div>

    <%@ include file="footer.jsp"%>

</body>
</html>