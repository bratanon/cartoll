<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta content="NO-CACHE" http-equiv="PRAGMA">
<meta content="NO-CACHE" http-equiv="CACHE-CONTROL">
<meta charset="utf-8" />
<link rel="stylesheet" href="<%=request.getContextPath() %>/theme/styles/reset.css" />
<link rel="stylesheet" href="<%=request.getContextPath() %>/theme/styles/style.css" />
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans|Inder"/>