<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="footer">
  <div class="footerImagesRight"></div>
  <div class="footerContent clearfix">
    <div class="footerIntro">
      <p>Passagejakt.se tillhandahåller ett utsökt gränssnitt för att administrera passager i ett system av världsklass (Klass GBJU13). </p>
    </div>

    <div class="footerLinks">
      <a href="#">Kontakt &amp; info</a>&nbsp;&nbsp;|&nbsp;&nbsp;
      <a href="#">För företag</a>&nbsp;&nbsp;|&nbsp;&nbsp;
      <a href="#">Sajtkarta</a>&nbsp;&nbsp;|&nbsp;&nbsp;
      <a href="#">Mmm cookies</a>
    </div>
  </div>
</div>