<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<%@ include file="head.jsp"%>
<title>Add Station - Passagejakt</title>
</head>
<body>
    <%@ include file="header.jsp"%>

    <div id="allContent">
        <h1>Lägg till passage</h1>
        <div class="pageDescription">
            <p>Kombinera din ultimata passage och skicka den till oss på passagejakt.</p>

            <form:form commandName="passageBean" method="post">

                <div class="form-item">
                    <label for="vehcileId">Fordon</label>
                    <form:select path="vehcileId" items="${vehicles}" />
                </div>

                <div class="form-item">
                    <label for="stationId">Station</label>
                    <form:select path="stationId" items="${stations}" />
                </div>
                
                <div class="form-item">
                    <label for="hour">Tid (Timme)</label>
                   <spring:bind path="hour">
                        <input id="hour" name="hour" type="number" min="0" max="23" value="0" />
                    </spring:bind>
                </div>
                
                <div class="form-item">
                    <label for="minute">Tid (Minut)</label>

                    <spring:bind path="minute">
                        <input id="minute" name="minute" type="number" min="0" max="59" value="0" />
                    </spring:bind>
                </div>

                <input type="submit" value="Lägg till">
                
            </form:form>

        </div>
    </div>

    <%@ include file="footer.jsp"%>

</body>
</html>