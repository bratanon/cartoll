<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="carTollTag" tagdir="/WEB-INF/tags"%>

<!DOCTYPE html>
<html>
    <head>
        <%@ include file="head.jsp"%>
        <title>Passagejakt</title>
    </head>
<body>
    <%@ include file="header.jsp"%>

    <div id="allContent">
        <h1>Välkommen till passagejakt!</h1>
        <div class="pageDescription">
            <p>Här hittar du passageförslag för alla åldrar och
                tillfällen.</p>

            <ul>
                <li>Totalt antal passager: ${totalPassages} st</li>
                <li>Total intäkt: ${totalIncome} kr</li>
            </ul>

            <div class="clearfix">
                <carTollTag:station stations="${stationList}"></carTollTag:station>
            </div>

            <div class="paginationDiv clearfix">
                <ul id="pagination">
                    <li class="active">1</li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li><a href="#">8</a></li>
                    <li><a href="#">9</a></li>
                    <li><a href="#">10</a></li>
                </ul>
            </div>

        </div>
    </div>

    <%@ include file="footer.jsp"%>

</body>
</html>