<%@tag description="Station view" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="stations" required="true" type="java.util.List"%>
<c:if test="${not empty statisticList}">
    <c:forEach items="${statisticList}" var="stats">
        <spring:url value="station/${stats.station.id}.html" var="stationUrl" />
        <div class="stationContainer clearfix">
            <h2><a href="${stationUrl}">${stats.station.name}</a></h2>
            <img src="${pageContext.request.contextPath}/theme/images/station.png" />
            <div class="stationinfo">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus hendrerit eros eu pulvinar posuere. Quisque condimentum ut dui vitae tempus.</p>
                <ul>
                    <li>Antal passager: ${stats.statistic.passageQuantity} st</li>
                    <li>Intäkt ${stats.statistic.income} kr</li>
                </ul>
                <a href="${stationUrl}"><img src="http://www.presentjakt.se/portalimages/buttons/las_mer.png"></a>
            </div>
        </div>
    </c:forEach>
</c:if>