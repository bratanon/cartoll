package se.stjerneman.cartoll.tags;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import se.stjerneman.cartoll.domain.station.Station;

public class StationTag extends SimpleTagSupport {

    private ArrayList<Station> stations;

    public ArrayList<Station> getStations() {
        return stations;
    }

    public void setStations(ArrayList<Station> stations) {
        this.stations = stations;
    }

    @Override
    public void doTag() throws JspException, IOException {
        getJspContext().setAttribute("stations", stations);
        getJspBody().invoke(null);
    }
}
