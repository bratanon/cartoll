package se.stjerneman.cartoll.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.stjerneman.cartoll.domain.owner.Owner;
import se.stjerneman.cartoll.domain.statistic.Statistic;
import se.stjerneman.cartoll.domain.vehicle.Vehicle;
import se.stjerneman.cartoll.service.owner.OwnerService;
import se.stjerneman.cartoll.service.passage.PassageService;
import se.stjerneman.cartoll.service.vehicle.VehicleService;
import se.stjerneman.cartoll.wrapper.StatisticWrapper;

@Controller
public class OwnerController {
    private Logger log = Logger.getLogger(OwnerController.class);

    @Autowired
    private OwnerService ownerService;

    @Autowired
    private PassageService passageService;

    @Autowired
    private VehicleService vehicleService;

    @RequestMapping("/owner/{ownerId}")
    public ModelAndView viewOnwer(@PathVariable long ownerId) {
        log.debug("Entering OwnerController.");

        Owner owner = ownerService.load(ownerId);

        List<StatisticWrapper> statisticList = new ArrayList<>();

        for (Vehicle vehicle : vehicleService.loadAllOwnedBy(ownerId)) {
            Statistic stats = passageService.getVehicleStatistic(vehicle.getId());
            statisticList.add(new StatisticWrapper(stats, vehicle));
        }

        ModelAndView mav = new ModelAndView("viewOwner");
        mav.addObject("owner", owner);
        mav.addObject("itemWrapper", statisticList);
        return mav;
    }
}
