package se.stjerneman.cartoll.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.stjerneman.cartoll.domain.station.Station;
import se.stjerneman.cartoll.domain.statistic.Statistic;
import se.stjerneman.cartoll.service.passage.PassageService;
import se.stjerneman.cartoll.service.station.StationService;
import se.stjerneman.cartoll.wrapper.StatisticWrapper;

@Controller
public class IndexController {
    private Logger log = Logger.getLogger(IndexController.class);

    @Autowired
    private StationService stationService;

    @Autowired
    private PassageService passageService;

    @RequestMapping("index")
    public ModelAndView index() {
        log.debug("Entering IndexController.");

        List<StatisticWrapper> statisticList = new ArrayList<>();

        int totaltPassages = 0;
        int totalIncome = 0;

        for (Station station : stationService.loadAll()) {
            Statistic stats = passageService.getStationStatistic(station.getId());
            statisticList.add(new StatisticWrapper(stats, station));

            totaltPassages += stats.getPassageQuantity();
            totalIncome += stats.getIncome();
        }

        ModelAndView mav = new ModelAndView("index");
        mav.addObject("statisticList", statisticList);
        mav.addObject("totalPassages", totaltPassages);
        mav.addObject("totalIncome", totalIncome);

        return mav;
    }
}
