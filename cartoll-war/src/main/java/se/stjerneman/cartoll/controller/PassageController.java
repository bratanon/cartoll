package se.stjerneman.cartoll.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.joda.time.MutableDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import se.stjerneman.cartoll.bean.PassageBean;
import se.stjerneman.cartoll.domain.passage.Passage;
import se.stjerneman.cartoll.domain.station.Station;
import se.stjerneman.cartoll.domain.vehicle.Vehicle;
import se.stjerneman.cartoll.service.passage.PassageService;
import se.stjerneman.cartoll.service.station.StationService;
import se.stjerneman.cartoll.service.vehicle.VehicleService;

@Controller
public class PassageController {
    private Logger log = Logger.getLogger(PassageController.class);

    @Autowired
    private VehicleService vehicleService;

    @Autowired
    private StationService stationService;

    @Autowired
    private PassageService passageService;

    @RequestMapping(value = "addPassage", method = RequestMethod.GET)
    public ModelAndView addPassageGet() {
        log.debug("Entering PassageController - addPassageGet.");

        PassageBean passageBean = new PassageBean();

        Map<Long, String> vehicles = getVehicleMap();
        Map<Long, String> stations = getStationMap();

        ModelAndView mav = new ModelAndView("addPassage");
        mav.addObject("passageBean", passageBean);
        mav.addObject("vehicles", vehicles);
        mav.addObject("stations", stations);
        return mav;
    }

    @RequestMapping(value = "addPassage", method = RequestMethod.POST)
    public ModelAndView addPassagePost(PassageBean passageBean) {
        log.debug("Entering PassageController - addPassagePost.");

        log.debug(passageBean);

        // Lean on this to not be null.
        Vehicle vehicle = vehicleService.load(passageBean.getVehcileId());
        Station station = stationService.load(passageBean.getStationId());

        MutableDateTime dateTime = new MutableDateTime();
        dateTime.setHourOfDay(passageBean.getHour());
        dateTime.setMinuteOfHour(passageBean.getMinute());

        Passage passage = new Passage(station, vehicle, dateTime.toDateTime());
        passageService.save(passage);
        log.debug(passage);
        ModelAndView mav = new ModelAndView("redirect:/passageAdded.html");
        mav.addObject("passageId", passage.getId());
        return mav;
    }

    @RequestMapping(value = "passageAdded")
    public ModelAndView passageAddedGet(@RequestParam long passageId) {
        log.debug("Entering PassageController - passageAddedGet.");
        log.debug(passageId);
        ModelAndView mav = new ModelAndView("passageAdded");
        mav.addObject("passage", passageService.load(passageId));
        return mav;
    }

    @RequestMapping("clearAllPassages")
    public ModelAndView clearAllPassages() {
        log.debug("PassageController PassageController - clearAllPassages.");
        return new ModelAndView("redirect:/");
    }

    private Map<Long, String> getVehicleMap() {
        Map<Long, String> vehicleMap = new LinkedHashMap<>();
        vehicleMap.put(0l, "-- SELECT --");

        for (Vehicle vehicle : vehicleService.loadAll()) {
            vehicleMap.put(vehicle.getId(), vehicle.getVehicleInformationString());
        }

        return vehicleMap;
    }

    private Map<Long, String> getStationMap() {
        Map<Long, String> stationMap = new LinkedHashMap<>();
        stationMap.put(0l, "-- SELECT --");

        for (Station station : stationService.loadAll()) {
            stationMap.put(station.getId(), station.getName());
        }

        return stationMap;
    }
}
