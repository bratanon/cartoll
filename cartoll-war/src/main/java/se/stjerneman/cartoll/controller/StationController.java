package se.stjerneman.cartoll.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.stjerneman.cartoll.domain.passage.Passage;
import se.stjerneman.cartoll.domain.station.Station;
import se.stjerneman.cartoll.service.passage.PassageService;
import se.stjerneman.cartoll.service.station.StationService;

@Controller
public class StationController {

    private static Logger log = Logger.getLogger(StationController.class);

    @Autowired
    private StationService stationService;

    @Autowired
    private PassageService passageService;

    @RequestMapping("/station/{stationId}")
    public ModelAndView viewStation(@PathVariable long stationId) {
        log.debug("viewStation");

        Station station = stationService.load(stationId);
        List<Passage> passages = passageService.loadAllByStationId(station.getId());

        ModelAndView mav = new ModelAndView("viewStation");
        mav.addObject("station", station);
        mav.addObject("passages", passages);
        return mav;
    }
}
