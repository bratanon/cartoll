package se.stjerneman.cartoll.wrapper;

import se.stjerneman.cartoll.domain.station.Station;
import se.stjerneman.cartoll.domain.statistic.Statistic;
import se.stjerneman.cartoll.domain.vehicle.Vehicle;

public class StatisticWrapper {

    private Statistic statistic;

    private Station station;

    private Vehicle vehicle;

    public StatisticWrapper (Statistic statistic, Station station) {
        this.statistic = statistic;
        this.station = station;
    }

    public StatisticWrapper (Statistic statistic, Vehicle vehicle) {
        this.statistic = statistic;
        this.vehicle = vehicle;
    }

    public Statistic getStatistic() {
        return statistic;
    }

    public void setStatistic(Statistic statistic) {
        this.statistic = statistic;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

}
