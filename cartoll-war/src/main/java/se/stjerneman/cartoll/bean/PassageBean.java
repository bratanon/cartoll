package se.stjerneman.cartoll.bean;

public class PassageBean {

    private int vehcileId;

    private int stationId;

    private int hour;

    private int minute;

    public int getVehcileId() {
        return vehcileId;
    }

    public void setVehcileId(int vehcileId) {
        this.vehcileId = vehcileId;
    }

    public int getStationId() {
        return stationId;
    }

    public void setStationId(int stationId) {
        this.stationId = stationId;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    @Override
    public String toString() {
        return "PassageBean [vehcileId=" + vehcileId + ", stationId=" + stationId + ", hour=" + hour + ", minute=" + minute + "]";
    }

}
