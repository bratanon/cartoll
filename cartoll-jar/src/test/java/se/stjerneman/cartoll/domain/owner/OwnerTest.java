package se.stjerneman.cartoll.domain.owner;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import se.stjerneman.cartoll.domain.vehicle.Car;

public class OwnerTest {

    @Test
    public void testCreateOwner() {
        String firstName = "John";
        String lastName = "Doe";
        String address = "Boulevard Ave.";
        String zipCode = "12345";
        String city = "Anytown";

        Owner owner = new Owner(firstName, lastName, address, zipCode, city);
        assertEquals("First name is set.", firstName, owner.getFirstName());
        assertEquals("Last name is set.", lastName, owner.getLastName());
        assertEquals("Address is set.", address, owner.getAddress());
        assertEquals("Zip code is set.", zipCode, owner.getZipCode());
        assertEquals("City is set.", city, owner.getCity());
    }

    @Test
    public void testOwnerVehicleList() {
        Owner owner = new Owner("John", "Doe");
        assertEquals("List of vehicles is empty", 0, owner.getVehicleList().size());

        owner.addVehicleToOwner(new Car("AAA-111", owner));
        assertEquals("List of vehicles is updated", 1, owner.getVehicleList().size());
    }
}
