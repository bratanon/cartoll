package se.stjerneman.cartoll.domain.vehicle;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import se.stjerneman.cartoll.domain.owner.Owner;

public class TruckTest {

    Owner owner;
    Truck truck;

    @Before
    public void setup() {
        owner = new Owner("John", "Doe");
        truck = new Truck("ABC-123", owner, 7000);
    }

    @Test
    public void testTruck() {
        String regNo = "ABC-123";
        float weight = 7000;
        Truck truck = new Truck(regNo, owner, weight);

        assertEquals("RegNo successfully loaded", regNo, truck.getRegNo());
        assertNotNull("Truck owner successfully loaded", truck.getOwner());
        assertEquals("Weight successfully loaded", weight, truck.getWeight(), 0);
    }

    @Test
    public void testCalculateOverWeight() {
        truck.setWeight(7000);
        assertEquals("Over weight", 1, truck.calculateOverWeight(), 0);
        truck.setWeight(7400);
        assertEquals("Over weight", 1, truck.calculateOverWeight(), 0);
        truck.setWeight(7500);
        assertEquals("Over weight", 1, truck.calculateOverWeight(), 0);
        truck.setWeight(7600);
        assertEquals("Over weight", 1, truck.calculateOverWeight(), 0);
        truck.setWeight(8100);
        assertEquals("Over weight", 2, truck.calculateOverWeight(), 0);
    }

    @Test
    public void testTruckFee() {
        testTruckFee("low", 12);
        testTruckFee("medium", 20);
        testTruckFee("high", 25);
        testTruckFee("free", 0);
    }

    public void testTruckFee(String level, int expected) {
        Truck truck = new Truck("AAA-111", owner, 5500);
        assertEquals("Truck - " + level + " cost", expected, truck.getPassageFee(level));

        // Test with 2000 kilograms over weight.
        truck.setWeight(Truck.WEIGHTLIMIT + 2000);
        assertEquals("Truck - " + level + " cost over weight", expected + (Truck.OVERWEIGHTFEE * 2), truck.getPassageFee(level));
    }

}
