package se.stjerneman.cartoll.domain.station;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class StationTest {

    @Test
    public void testCreateStation() {
        String stationName = "Fridkullagatan";

        Station station = new Station(stationName);

        assertNotNull("Station is not null", station);
        assertEquals("Station name is set", stationName, station.getName());
    }
}
