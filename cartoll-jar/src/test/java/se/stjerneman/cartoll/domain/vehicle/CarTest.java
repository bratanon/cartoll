package se.stjerneman.cartoll.domain.vehicle;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import se.stjerneman.cartoll.domain.owner.Owner;

public class CarTest {

    Owner owner;

    @Before
    public void setup() {
        owner = new Owner("John", "Doe");
    }

    @Test
    public void testCarConstructor() {
        String regNo = "ABC-123";
        Car car = new Car(regNo, owner);

        assertNotNull("Car is loaded", car);
        assertNotNull("Car owner loaded", car.getOwner());
        assertEquals("Car regno", regNo, car.getRegNo());
    }

    @Test
    public void testCarFee() {
        testCarFee("low", 8);
        testCarFee("medium", 13);
        testCarFee("high", 18);
        testCarFee("free", 0);
    }

    public void testCarFee(String level, int expected) {
        Car car = new Car("AAA-111", owner);
        assertEquals("Car - " + level + " cost", expected, car.getPassageFee(level));
    }
}
