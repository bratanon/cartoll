package se.stjerneman.cartoll.domain.vehicle;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import se.stjerneman.cartoll.domain.owner.Owner;

public class TaxiTest {

    Owner owner;

    @Before
    public void setup() {
        owner = new Owner("John", "Doe");
    }

    @Test
    public void testTaxiConstructor() {
        String regNo = "ABC-123";
        Taxi taxi = new Taxi(regNo, owner);

        assertNotNull("Taxi is loaded", taxi);
        assertEquals("Taxi owner", owner, taxi.getOwner());
        assertEquals("Taxi regno", regNo, taxi.getRegNo());

        // Default shall be false.
        assertFalse("Taxi is not eco certified", taxi.isEcoCertified());
    }

    @Test
    public void testTaxiEcoTrue() {
        Taxi taxi = new Taxi("AAA-111", owner, true);
        assertTrue("Taxi is eco certified", taxi.isEcoCertified());
    }

    @Test
    public void testTaxiEcoFalse() {
        Taxi taxi = new Taxi("AAA-111", owner, false);
        assertFalse("Taxi is not eco certified", taxi.isEcoCertified());
    }

    @Test
    public void testTaxiFee() {
        testTaxiFee("low", 15, 10);
        testTaxiFee("medium", 20, 15);
        testTaxiFee("high", 25, 20);
        testTaxiFee("free", 0, 0);
    }

    public void testTaxiFee(String level, int expected, int expected_eco) {
        Taxi taxi = new Taxi("AAA-111", owner);
        assertEquals("Taxi - " + level + " cost", expected, taxi.getPassageFee(level));

        // Test with eco.
        taxi.setEcoCertified(true);
        assertEquals("Taxi - " + level + " cost eco", expected_eco, taxi.getPassageFee(level));
    }
}
