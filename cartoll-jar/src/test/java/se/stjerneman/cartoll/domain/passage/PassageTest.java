package se.stjerneman.cartoll.domain.passage;

import static org.junit.Assert.assertNotNull;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import se.stjerneman.cartoll.domain.owner.Owner;
import se.stjerneman.cartoll.domain.station.Station;
import se.stjerneman.cartoll.domain.vehicle.Car;
import se.stjerneman.cartoll.domain.vehicle.Taxi;
import se.stjerneman.cartoll.domain.vehicle.Truck;

public class PassageTest {

    Station station;
    Car car;
    Taxi taxi;
    Truck truck;

    @Before
    public void setup() {
        station = new Station("Fridkullagatan");
        Owner owner = new Owner("John", "Doe");
        car = new Car("AAA-111", owner);
        taxi = new Taxi("BBB-222", owner);
        truck = new Truck("CCC-333", owner, 6);
    }

    @Test
    public void testCreatePassage() {
        DateTime passDate = new DateTime(2014, 1, 28, 12, 0);
        Passage passage = new Passage(station, car, passDate);

        assertNotNull("Passage has a station", passage.getStation());
        assertNotNull("Passage has a vehicle", passage.getVehicle());
    }

}
