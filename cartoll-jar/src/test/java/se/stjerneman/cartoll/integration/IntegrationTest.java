package se.stjerneman.cartoll.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import se.stjerneman.cartoll.domain.owner.Owner;
import se.stjerneman.cartoll.domain.passage.Passage;
import se.stjerneman.cartoll.domain.station.Station;
import se.stjerneman.cartoll.domain.statistic.Statistic;
import se.stjerneman.cartoll.domain.vehicle.Car;
import se.stjerneman.cartoll.domain.vehicle.Taxi;
import se.stjerneman.cartoll.domain.vehicle.Truck;
import se.stjerneman.cartoll.domain.vehicle.Vehicle;
import se.stjerneman.cartoll.service.owner.OwnerService;
import se.stjerneman.cartoll.service.passage.PassageService;
import se.stjerneman.cartoll.service.station.StationService;
import se.stjerneman.cartoll.service.vehicle.VehicleService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/applicationContext_mock.xml" })
public class IntegrationTest {

    @Autowired
    private OwnerService ownerService;

    @Autowired
    private StationService stationService;

    @Autowired
    private PassageService passageService;

    @Autowired
    private VehicleService vehicleService;

    @Test
    public void testIntegration() {
        // Create owners.
        Owner owner1 = ownerService.save(new Owner("Foo", "Foosson"));
        Owner owner2 = ownerService.save(new Owner("Bar", "Barsson"));
        Owner owner3 = ownerService.save(new Owner("Baz", "Bazsson"));
        assertNotNull("Owner 1 created.", owner1);
        assertNotNull("Owner 2 created.", owner2);
        assertNotNull("Owner 3 created.", owner3);
        assertEquals("The totalt amount of owners is 3.", 3, ownerService.loadAll().size());

        // Delete owners.
        ownerService.delete(owner2.getId());
        assertNull("Owner 2 deleted.", ownerService.load(owner2.getId()));
        assertEquals("The totalt amount of owners is 2.", 2, ownerService.loadAll().size());

        // Create stations.
        Station station1 = stationService.save(new Station("FooStation"));
        Station station2 = stationService.save(new Station("BarStation"));
        Station station3 = stationService.save(new Station("BazStation"));
        assertNotNull("Station 1 created.", station1);
        assertNotNull("Station 2 created.", station2);
        assertNotNull("Station 3 created.", station3);
        assertEquals("The totalt amount of stations is 3.", 3, stationService.loadAll().size());

        // Delete stations.
        stationService.delete(station3.getId());
        assertNull("Station 1 deleted.", stationService.load(station3.getId()));
        assertEquals("The totalt amount of stations is 2.", 2, stationService.loadAll().size());

        // Create vehicles.
        Vehicle car = vehicleService.save(new Car("AAA-111", owner1));
        Vehicle car2 = vehicleService.save(new Car("AAA-999", owner1));
        Vehicle taxiNonEco = vehicleService.save(new Taxi("BBB-111", owner1, false));
        Vehicle taxiEco = vehicleService.save(new Taxi("BBB-222", owner1, true));
        Vehicle truckLight = vehicleService.save(new Truck("CCC-111", owner1, 5000));
        Vehicle truckHeavy = vehicleService.save(new Truck("CCC-222", owner1, 8500));
        assertNotNull("car is created.", car);
        assertNotNull("car2 is created.", car2);
        assertNotNull("taxiNonEco created.", taxiNonEco);
        assertNotNull("taxiEco created.", taxiEco);
        assertNotNull("truckLight created.", truckLight);
        assertNotNull("truckHeavy created.", truckHeavy);
        assertEquals("The totalt amount of vehicles is 6.", 6, vehicleService.loadAll().size());

        // Delete vehicle.
        vehicleService.delete(car2.getId());
        assertNull("car2 deleted.", vehicleService.load(car2.getId()));
        assertEquals("The totalt amount of vehicles is 5.", 5, vehicleService.loadAll().size());

        assertEquals("All vehicles owned by owner 1 loaded.", 5, vehicleService.loadAllOwnedBy(owner1.getId()).size());

        // DateTime passDate = new LocalTime(2014, 1, 28, 12, 0);

        DateTime datetime1 = new LocalTime(7, 0).toDateTimeToday();
        DateTime datetime2 = new LocalTime(9, 0).toDateTimeToday();
        DateTime datetime3 = new LocalTime(11, 0).toDateTimeToday();
        DateTime datetime4 = new LocalTime(13, 0).toDateTimeToday();
        DateTime datetime5 = new LocalTime(15, 0).toDateTimeToday();
        DateTime datetime6 = new LocalTime(23, 30).toDateTimeToday();

        // Create passages.
        Passage passage1 = passageService.save(new Passage(station1, car, datetime1));
        Passage passage2 = passageService.save(new Passage(station1, taxiNonEco, datetime2));
        Passage passage3 = passageService.save(new Passage(station1, taxiEco, datetime3));
        Passage passage4 = passageService.save(new Passage(station2, truckLight, datetime4));
        Passage passage5 = passageService.save(new Passage(station2, truckHeavy, datetime5));
        Passage passage6 = passageService.save(new Passage(station2, car, datetime6));

        assertNotNull("Passage 1 created.", passage1);
        assertNotNull("Passage 2 created.", passage2);
        assertNotNull("Passage 3 created.", passage3);
        assertNotNull("Passage 4 created.", passage4);
        assertNotNull("Passage 5 created.", passage5);
        assertNotNull("Passage 6 created.", passage6);
        assertEquals("The totalt amount of passages is 6.", 6, passageService.loadAll().size());

        assertEquals("Price passage 1", 18, passage1.getCost());
        assertEquals("Price passage 2", 15, passage2.getCost());
        assertEquals("Price passage 3", 10, passage3.getCost());
        assertEquals("Price passage 4", 12, passage4.getCost());
        assertEquals("Price passage 5", 40, passage5.getCost());

        // Delete passage.
        passageService.delete(passage6.getId());
        assertNull("Passage 6 deleted.", passageService.load(passage6.getId()));
        assertEquals("The totalt amount of passages is 5.", 5, passageService.loadAll().size());

        // Statistics
        Statistic statsStation1 = passageService.getStationStatistic(station1.getId());
        assertEquals("Station stats - income", 43, statsStation1.getIncome());
        assertEquals("Station stats - quantity", 3, statsStation1.getPassageQuantity());

        Statistic statsCar = passageService.getVehicleStatistic(car.getId());
        assertEquals("Vehcile stats - income", 18, statsCar.getIncome());
        assertEquals("Vehcile stats - quantity", 1, statsCar.getPassageQuantity());

    }
}
