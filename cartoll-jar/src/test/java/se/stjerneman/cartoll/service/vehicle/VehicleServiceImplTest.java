package se.stjerneman.cartoll.service.vehicle;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import se.stjerneman.cartoll.domain.owner.Owner;
import se.stjerneman.cartoll.domain.vehicle.Car;
import se.stjerneman.cartoll.domain.vehicle.Vehicle;
import se.stjerneman.cartoll.repository.VehicleRepository;

public class VehicleServiceImplTest {

    private VehicleServiceImpl vehicleService = new VehicleServiceImpl();

    VehicleRepository vehicleRepository;

    @Before
    public void setUp() throws Exception {
        vehicleRepository = EasyMock.createMock(VehicleRepository.class);
        vehicleService.setRepository(vehicleRepository);
    }

    @After
    public void tearDown() throws Exception {
        vehicleRepository = null;
        vehicleService.setRepository(null);
    }

    @Test
    public void testAssertVehicleService() {
        assertNotNull("VehicleService exists", vehicleService);
    }

    @Test
    public void testSave() {
        Vehicle vehicle = new Car("AAA-111", new Owner("John", "Doe"));
        EasyMock.expect(vehicleRepository.save(vehicle)).andReturn(5l);
        EasyMock.expect(vehicleRepository.load(5)).andReturn(vehicle);
        EasyMock.replay(vehicleRepository);
        vehicleService.save(vehicle);
        EasyMock.verify(vehicleRepository);
    }

    @Test
    public void testLoad() {
        Vehicle vehicle = new Car("AAA-111", new Owner("John", "Doe"));
        EasyMock.expect(vehicleRepository.load(5)).andReturn(vehicle);
        EasyMock.replay(vehicleRepository);
        vehicleService.load(5);
        EasyMock.verify(vehicleRepository);
    }

    @Test
    public void testLoadAll() {
        EasyMock.expect(vehicleRepository.loadAll()).andReturn(new ArrayList<Vehicle>());
        EasyMock.replay(vehicleRepository);
        vehicleService.loadAll();
        EasyMock.verify(vehicleRepository);
    }

}
