package se.stjerneman.cartoll.service.passage;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;

import org.easymock.EasyMock;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import se.stjerneman.cartoll.domain.owner.Owner;
import se.stjerneman.cartoll.domain.passage.Passage;
import se.stjerneman.cartoll.domain.station.Station;
import se.stjerneman.cartoll.domain.vehicle.Car;
import se.stjerneman.cartoll.domain.vehicle.Vehicle;
import se.stjerneman.cartoll.repository.PassageRepository;

public class PassageServiceImplTest {

    private PassageServiceImpl passageService = new PassageServiceImpl();

    PassageRepository passageRepository;

    Vehicle car;
    Station station;

    @Before
    public void setUp() throws Exception {
        passageRepository = EasyMock.createMock(PassageRepository.class);
        passageService.setRepository(passageRepository);
        Owner owner = new Owner("John", "Doe");
        car = new Car("AAA-111", owner);
        station = new Station("FooStation");
    }

    @After
    public void tearDown() throws Exception {
        passageRepository = null;
        passageService.setRepository(null);
    }

    @Test
    public void testAssertPassageService() {
        assertNotNull("PassageService exists", passageService);
    }

    @Test
    public void testSave() {
        Passage passage = new Passage(station, car, new DateTime());
        EasyMock.expect(passageRepository.save(passage)).andReturn(5l);
        EasyMock.expect(passageRepository.load(5)).andReturn(passage);
        EasyMock.replay(passageRepository);
        passageService.save(passage);
        EasyMock.verify(passageRepository);
    }

    @Test
    public void testLoad() {
        Passage passage = new Passage(station, car, new DateTime());
        EasyMock.expect(passageRepository.load(5)).andReturn(passage);
        EasyMock.replay(passageRepository);
        passageService.load(5);
        EasyMock.verify(passageRepository);
    }

    @Test
    public void testLoadAll() {
        EasyMock.expect(passageRepository.loadAll()).andReturn(new ArrayList<Passage>());
        EasyMock.replay(passageRepository);
        passageService.loadAll();
        EasyMock.verify(passageRepository);
    }

}
