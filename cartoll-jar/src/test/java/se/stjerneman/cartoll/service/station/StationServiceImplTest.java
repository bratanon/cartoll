package se.stjerneman.cartoll.service.station;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import se.stjerneman.cartoll.domain.station.Station;
import se.stjerneman.cartoll.repository.StationRepository;

public class StationServiceImplTest {

    private StationServiceImpl stationService = new StationServiceImpl();

    StationRepository stationRepository;

    @Before
    public void setUp() throws Exception {
        stationRepository = EasyMock.createMock(StationRepository.class);
        stationService.setRepository(stationRepository);
    }

    @After
    public void tearDown() throws Exception {
        stationRepository = null;
        stationService.setRepository(null);
    }

    @Test
    public void testAssertStationService() {
        assertNotNull("StationService exists", stationService);
    }

    @Test
    public void testSave() {
        Station station = new Station("FooStation");
        EasyMock.expect(stationRepository.save(station)).andReturn(5l);
        EasyMock.expect(stationRepository.load(5)).andReturn(station);
        EasyMock.replay(stationRepository);
        stationService.save(station);
        EasyMock.verify(stationRepository);
    }

    @Test
    public void testLoad() {
        Station station = new Station("FooStation");
        EasyMock.expect(stationRepository.load(5)).andReturn(station);
        EasyMock.replay(stationRepository);
        stationService.load(5);
        EasyMock.verify(stationRepository);
    }

    @Test
    public void testLoadAll() {
        EasyMock.expect(stationRepository.loadAll()).andReturn(new ArrayList<Station>());
        EasyMock.replay(stationRepository);
        stationService.loadAll();
        EasyMock.verify(stationRepository);
    }

}
