package se.stjerneman.cartoll.service.owner;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import se.stjerneman.cartoll.domain.owner.Owner;
import se.stjerneman.cartoll.repository.OwnerRepository;

public class OwnerServiceImplTest {

    private OwnerServiceImpl ownerService = new OwnerServiceImpl();

    OwnerRepository ownerRepository;

    @Before
    public void setUp() throws Exception {
        // Setup mock repository.
        ownerRepository = EasyMock.createMock(OwnerRepository.class);
        ownerService.setRepository(ownerRepository);
    }

    @After
    public void tearDown() throws Exception {
        ownerRepository = null;
        ownerService.setRepository(null);
    }

    @Test
    public void testAssertOwnerService() {
        assertNotNull("OwnerService exists", ownerService);
    }

    @Test
    public void testSave() {
        Owner owner = new Owner("John", "Doe");
        EasyMock.expect(ownerRepository.save(EasyMock.eq(owner))).andReturn(5l);
        EasyMock.expect(ownerRepository.load(5)).andReturn(owner);
        EasyMock.replay(ownerRepository);
        ownerService.save(owner);
        EasyMock.verify(ownerRepository);
    }

    @Test
    public void testLoad() {
        Owner owner = new Owner("John", "Doe");
        EasyMock.expect(ownerRepository.load(5)).andReturn(owner);
        EasyMock.replay(ownerRepository);
        ownerService.load(5);
        EasyMock.verify(ownerRepository);
    }

    @Test
    public void testLoadAll() {
        EasyMock.expect(ownerRepository.loadAll()).andReturn(new ArrayList<Owner>());
        EasyMock.replay(ownerRepository);
        ownerService.loadAll();
        EasyMock.verify(ownerRepository);
    }
}
