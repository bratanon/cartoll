package se.stjerneman.cartoll.service.owner;

import java.util.List;

import se.stjerneman.cartoll.domain.owner.Owner;

/**
 * A service class for handling owners.
 * 
 * @author Emil Stjerneman
 * 
 */
public interface OwnerService {

    /**
     * Saves an owner to a repository.
     * 
     * @param owner
     *            the owner to save.
     * @return the saved owner.
     */
    public Owner save(Owner owner);

    /**
     * Deletes an owner from a repository.
     * 
     * @param id
     *            the owner id to delete.
     */
    public void delete(long id);

    /**
     * Deletes all owners from the repository;
     */
    public void deleteAll();

    /**
     * Loads an owner from the repository.
     * 
     * @param id
     *            the id of the owner to load.
     * @return an owner.
     */
    public Owner load(long id);

    /**
     * Loads all owners from the repository.
     * 
     * @return a list of all loaded owners.
     */
    public List<Owner> loadAll();
}
