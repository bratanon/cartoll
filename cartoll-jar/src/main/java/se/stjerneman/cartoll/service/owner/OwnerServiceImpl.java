package se.stjerneman.cartoll.service.owner;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.stjerneman.cartoll.domain.owner.Owner;
import se.stjerneman.cartoll.repository.OwnerRepository;

@Service
public class OwnerServiceImpl implements OwnerService {

    private static Logger log = Logger.getLogger(OwnerServiceImpl.class);

    @Autowired
    private OwnerRepository ownerRepository;

    public void setRepository(OwnerRepository ownerRepository) {
        this.ownerRepository = ownerRepository;
    }

    @Override
    public Owner save(Owner owner) {
        log.debug("Saving owner : " + owner);
        long id = ownerRepository.save(owner);
        Owner loaded_owner = ownerRepository.load(id);
        log.debug("Saved owner : " + loaded_owner);
        return loaded_owner;
    }

    @Override
    public void delete(long id) {
        log.debug("Delete owner with id: " + id);
        ownerRepository.delete(id);
    }

    @Override
    public void deleteAll() {
        log.debug("Delete all owners.");
        ownerRepository.deleteAll();
    }

    @Override
    public Owner load(long id) {
        log.debug("Load owner with id: " + id);
        return ownerRepository.load(id);
    }

    @Override
    public List<Owner> loadAll() {
        log.debug("Load all owners.");
        return ownerRepository.loadAll();
    }
}
