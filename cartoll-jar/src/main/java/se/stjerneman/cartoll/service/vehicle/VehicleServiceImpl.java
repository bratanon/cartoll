package se.stjerneman.cartoll.service.vehicle;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.stjerneman.cartoll.domain.vehicle.Vehicle;
import se.stjerneman.cartoll.repository.VehicleRepository;

@Service
public class VehicleServiceImpl implements VehicleService {

    private static Logger log = Logger.getLogger(VehicleServiceImpl.class);

    @Autowired
    VehicleRepository vehicleRepository;

    public void setRepository(VehicleRepository vehicleRepository) {
        this.vehicleRepository = vehicleRepository;
    }

    @Override
    public Vehicle save(Vehicle vehicle) {
        log.debug("Saving vehicle : " + vehicle);
        long id = vehicleRepository.save(vehicle);
        Vehicle loaded_vehicle = vehicleRepository.load(id);
        log.debug("Saved vehicle : " + loaded_vehicle);
        return loaded_vehicle;

    }

    @Override
    public void delete(long id) {
        log.debug("Delete vehicle with id: " + id);
        vehicleRepository.delete(id);
    }

    @Override
    public void deleteAll() {
        log.debug("Delete all vehicles.");
        vehicleRepository.deleteAll();
    }

    @Override
    public Vehicle load(long id) {
        log.debug("Load vehicle with id: " + id);
        return vehicleRepository.load(id);
    }

    @Override
    public List<Vehicle> loadAll() {
        log.debug("Load all vehicles.");
        return vehicleRepository.loadAll();
    }

    @Override
    public List<Vehicle> loadAllOwnedBy(long owner_id) {
        log.debug("Load all vehicles that is owned by " + owner_id);
        return vehicleRepository.loadAllOwnedBy(owner_id);
    }
}
