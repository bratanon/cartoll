package se.stjerneman.cartoll.service.passage;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.stjerneman.cartoll.domain.passage.Passage;
import se.stjerneman.cartoll.domain.statistic.Statistic;
import se.stjerneman.cartoll.repository.PassageRepository;

@Service
public class PassageServiceImpl implements PassageService {

    private static Logger log = Logger.getLogger(PassageServiceImpl.class);

    @Autowired
    private PassageRepository passageRepository;

    public void setRepository(PassageRepository passageRepository) {
        this.passageRepository = passageRepository;
    }

    @Override
    public Passage save(Passage passage) {
        log.debug("Saving passage : " + passage);
        long id = passageRepository.save(passage);
        Passage loaded_passage = passageRepository.load(id);
        log.debug("Saved passage : " + loaded_passage);
        return loaded_passage;
    }

    @Override
    public void delete(long id) {
        log.debug("Delete passage with id: " + id);
        passageRepository.delete(id);
    }

    @Override
    public void deleteAll() {
        log.debug("Delete all passages.");
        passageRepository.deleteAll();
    }

    @Override
    public Passage load(long id) {
        log.debug("Load passage with id: " + id);
        return passageRepository.load(id);
    }

    @Override
    public List<Passage> loadAll() {
        log.debug("Load all passages.");
        return passageRepository.loadAll();
    }

    @Override
    public List<Passage> loadAllByStationId(long station_id) {
        return passageRepository.loadAllByStationId(station_id);
    }

    @Override
    // TODO: Check if list is null?
    public Statistic getStationStatistic(long station_id) {
        return passageRepository.getStationStatistic(station_id);
    }

    @Override
    // TODO: Check if list is null?
    public Statistic getVehicleStatistic(long vehicle_id) {
        return passageRepository.getVehicleStatistic(vehicle_id);
    }
}
