package se.stjerneman.cartoll.service.station;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.stjerneman.cartoll.domain.station.Station;
import se.stjerneman.cartoll.repository.StationRepository;

@Service
public class StationServiceImpl implements StationService {

    private static Logger log = Logger.getLogger(StationServiceImpl.class);

    @Autowired
    private StationRepository stationRepository;

    public void setRepository(StationRepository stationRepository) {
        this.stationRepository = stationRepository;
    }

    @Override
    public Station save(Station station) {
        log.debug("Saving station : " + station);
        long id = stationRepository.save(station);
        Station loaded_station = stationRepository.load(id);
        log.debug("Saved station : " + loaded_station);
        return loaded_station;
    }

    @Override
    public void delete(long id) {
        log.debug("Delete station with id: " + id);
        stationRepository.delete(id);
    }

    @Override
    public void deleteAll() {
        log.debug("Delete all stations.");
        stationRepository.deleteAll();
    }

    @Override
    public Station load(long id) {
        log.debug("Load station with id: " + id);
        return stationRepository.load(id);
    }

    @Override
    public List<Station> loadAll() {
        log.debug("Load all stations.");
        return stationRepository.loadAll();
    }
}
