package se.stjerneman.cartoll.service.vehicle;

import java.util.List;

import se.stjerneman.cartoll.domain.vehicle.Vehicle;

/**
 * A service class for handling vehicles.
 * 
 * @author Emil Stjerneman
 * 
 */
public interface VehicleService {
    /**
     * Saves a vehicle to a repository.
     * 
     * @param owner
     *            the vehicle to save.
     * @return the saved vehicle.
     */
    public Vehicle save(Vehicle vehicle);

    /**
     * Deletes a vehicle from a repository.
     * 
     * @param id
     *            the vehicle id to delete.
     */
    public void delete(long id);

    /**
     * Deletes all vehicles from the repository;
     */
    public void deleteAll();

    /**
     * Loads a vehicle from a repository.
     * 
     * @param id
     *            the id of the vehicle to load.
     * @return a vehicle.
     */
    public Vehicle load(long id);

    /**
     * Loads all vehicles from a repository.
     * 
     * @return a list of all loaded vehicles.
     */
    public List<Vehicle> loadAll();

    /**
     * Loads all vehicles from a repository owned by the given owner id.
     * 
     * @param owner_id
     *            the owner to filer vehicles on.
     * @return a list of all loaded vehicles owned by the given owner id.
     */
    public List<Vehicle> loadAllOwnedBy(long owner_id);
}
