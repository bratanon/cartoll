package se.stjerneman.cartoll.service.station;

import java.util.List;

import se.stjerneman.cartoll.domain.station.Station;

/**
 * A service class for handling stations.
 * 
 * @author Emil Stjerneman
 * 
 */
public interface StationService {

    /**
     * Saves a station to a repository.
     * 
     * @param owner
     *            the station to save.
     * @return the saved station.
     */
    public Station save(Station station);

    /**
     * Deletes a station from a repository.
     * 
     * @param id
     *            the station id to delete.
     */
    public void delete(long id);

    /**
     * Deletes all stations from the repository;
     */
    public void deleteAll();

    /**
     * Loads a station from a repository.
     * 
     * @param id
     *            the id of the station to load.
     * @return a station.
     */
    public Station load(long id);

    /**
     * Loads all stations from a repository.
     * 
     * @return a list of all loaded stations.
     */
    public List<Station> loadAll();
}
