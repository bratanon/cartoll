package se.stjerneman.cartoll.service.passage;

import java.util.List;

import se.stjerneman.cartoll.domain.passage.Passage;
import se.stjerneman.cartoll.domain.statistic.Statistic;

/**
 * A service class for handling passages.
 * 
 * @author Emil Stjerneman
 * 
 */
public interface PassageService {

    /**
     * Saves a passage to a repository.
     * 
     * @param passage
     *            the passage to save.
     * @return the saved passage.
     */
    public Passage save(Passage passage);

    /**
     * Deletes a passage from a repository.
     * 
     * @param id
     *            the id of passage to delete.
     */
    public void delete(long id);

    /**
     * Deletes all passages from the repository;
     */
    public void deleteAll();

    /**
     * Loads a passage from a repository.
     * 
     * @param id
     *            the id of the passage to load.
     * @return a passage.
     */
    public Passage load(long id);

    /**
     * Loads all passages from a repository.
     * 
     * @return a list of all loaded passages.
     */
    public List<Passage> loadAll();

    /**
     * Loads all passages for a station from a repository.
     * 
     * @param station_id
     *            the station is to filter the query on.
     * 
     * @return a list of passages for a station.
     */
    public List<Passage> loadAllByStationId(long station_id);

    /**
     * Gets statistics for a given station. The statistics can, but most not
     * include both total income and passage quantity.
     * 
     * @param station_id
     *            the station id to filer the query on.
     * @return a populated statistic object.
     */
    public Statistic getStationStatistic(long station_id);

    /**
     * Gets statistics for a given vehicle. The statistics can, but most not
     * include both total income and passage quantity.
     * 
     * @param vehicle_id
     *            the vehicle id to filer the query on.
     * @return a populated statistic object.
     */
    public Statistic getVehicleStatistic(long vehicle_id);
}
