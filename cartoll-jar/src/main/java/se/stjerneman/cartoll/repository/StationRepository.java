package se.stjerneman.cartoll.repository;

import java.util.List;

import se.stjerneman.cartoll.domain.station.Station;

/**
 * A repository class for handling stations in a repository.
 * 
 * @author Emil Stjerneman
 * 
 */
public interface StationRepository {

    /**
     * Permanently saves the given station.
     * 
     * @param station
     *            the station to save.
     * @return the id of the saved station.
     */
    public long save(Station station);

    /**
     * Permanently deletes a saved station.
     * 
     * @param id
     *            the id of the station to delete.
     */
    public void delete(long id);

    /**
     * Delete all stored stations.
     */
    public void deleteAll();

    /**
     * Loads a stored station.
     * 
     * @param id
     *            the id of the station to load.
     * @return a station.
     */
    public Station load(long id);

    /**
     * Loads all stored stations.
     * 
     * @return a list of all stored stations.
     */
    public List<Station> loadAll();
}
