package se.stjerneman.cartoll.repository.mock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import se.stjerneman.cartoll.domain.vehicle.Vehicle;
import se.stjerneman.cartoll.repository.VehicleRepository;

public class MockVehicleRepository implements VehicleRepository {

    private static long vehicleId = 1;

    private Map<Long, Vehicle> vehicles = new HashMap<>();

    @Override
    public long save(Vehicle vehicle) {
        vehicle.setId(vehicleId++);
        vehicles.put(vehicle.getId(), vehicle);
        return vehicle.getId();
    }

    @Override
    public void delete(long id) {
        vehicles.remove(id);
    }

    @Override
    public Vehicle load(long id) {
        return vehicles.get(id);
    }

    @Override
    public List<Vehicle> loadAll() {
        return new ArrayList<>(vehicles.values());
    }

    @Override
    public void deleteAll() {
        vehicles.clear();
    }

    @Override
    public List<Vehicle> loadAllOwnedBy(long owner_id) {
        List<Vehicle> vehicles = new ArrayList<>();

        for (Vehicle vehicle : loadAll()) {
            if (vehicle.getOwner().getId() == owner_id) {
                vehicles.add(vehicle);
            }
        }
        return vehicles;
    }
}
