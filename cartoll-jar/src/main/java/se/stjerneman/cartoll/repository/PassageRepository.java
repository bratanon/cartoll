package se.stjerneman.cartoll.repository;

import java.util.List;

import se.stjerneman.cartoll.domain.passage.Passage;
import se.stjerneman.cartoll.domain.statistic.Statistic;

/**
 * A repository class for handling stations passages in a repository.
 * 
 * @author Emil Stjerneman
 * 
 */
public interface PassageRepository {

    /**
     * Permanently saves the given passage.
     * 
     * @param passage
     *            the passage to save.
     * @return the id of the saved passage.
     */
    public long save(Passage passage);

    /**
     * Permanently deletes a saved passage.
     * 
     * @param id
     *            the id of the passage to delete.
     */
    public void delete(long id);

    /**
     * Delete all stored passage.
     */
    public void deleteAll();

    /**
     * Loads a stored passage.
     * 
     * @param id
     *            the id of the passage to load.
     * @return a passage.
     */
    public Passage load(long id);

    /**
     * Loads all stored passages.
     * 
     * @return a list of all stored passages.
     */
    public List<Passage> loadAll();

    /**
     * Loads all stored passages for a station.
     * 
     * @param station_id
     *            the station is to filter the query on.
     * 
     * @return a list of all stored passages for a station.
     */
    public List<Passage> loadAllByStationId(long station_id);

    /**
     * Gets statistics for a given station. The statistics can, but most not
     * include both total income and passage quantity.
     * 
     * @param station_id
     *            the station id to filer the query on.
     * @return a populated statistic object.
     */
    public Statistic getStationStatistic(long station_id);

    /**
     * Gets statistics for a given vehicle. The statistics can, but most not
     * include both total income and passage quantity.
     * 
     * @param vehicle_id
     *            the vehicle id to filer the query on.
     * @return a populated statistic object.
     */
    public Statistic getVehicleStatistic(long vehicle_id);

}
