package se.stjerneman.cartoll.repository.mock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import se.stjerneman.cartoll.domain.station.Station;
import se.stjerneman.cartoll.repository.StationRepository;

public class MockStationRepository implements StationRepository {

    private static long stationId = 1;

    private Map<Long, Station> stations = new HashMap<>();

    @Override
    public long save(Station station) {
        station.setId(stationId++);
        stations.put(station.getId(), station);
        return station.getId();
    }

    @Override
    public void delete(long id) {
        stations.remove(id);
    }

    @Override
    public Station load(long id) {
        return stations.get(id);
    }

    @Override
    public List<Station> loadAll() {
        return new ArrayList<>(stations.values());
    }

    @Override
    public void deleteAll() {
        stations.clear();
    }
}
