package se.stjerneman.cartoll.repository.mock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import se.stjerneman.cartoll.domain.passage.Passage;
import se.stjerneman.cartoll.domain.statistic.Statistic;
import se.stjerneman.cartoll.repository.PassageRepository;

public class MockPassageRepository implements PassageRepository {

    private static long passageId = 1;

    private Map<Long, Passage> passages = new HashMap<>();

    @Override
    public long save(Passage passage) {
        passage.setId(passageId++);
        passages.put(passage.getId(), passage);
        return passage.getId();
    }

    @Override
    public void delete(long id) {
        passages.remove(id);
    }

    @Override
    public Passage load(long id) {
        return passages.get(id);
    }

    @Override
    public List<Passage> loadAll() {
        return new ArrayList<>(passages.values());
    }

    @Override
    public void deleteAll() {
        passages.clear();
    }

    @Override
    public List<Passage> loadAllByStationId(long station_id) {
        List<Passage> passages = new ArrayList<>();

        for (Passage passage : loadAll()) {
            if (passage.getStation().getId() == station_id) {
                passages.add(passage);
            }
        }
        return passages;
    }

    @Override
    public Statistic getStationStatistic(long station_id) {
        long passageQuantity = 0;
        long income = 0;

        for (Passage passage : loadAll()) {
            if (passage.getStation().getId() == station_id) {
                passageQuantity++;
                income += passage.getCost();
            }
        }

        return new Statistic(income, passageQuantity);
    }

    @Override
    public Statistic getVehicleStatistic(long vehicle_id) {
        long passageQuantity = 0;
        long income = 0;

        for (Passage passage : loadAll()) {
            if (passage.getVehicle().getId() == vehicle_id) {
                passageQuantity++;
                income += passage.getCost();
            }
        }

        return new Statistic(income, passageQuantity);
    }

}
