package se.stjerneman.cartoll.repository.jpa;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class JpaAbstractRepository {
    @PersistenceContext
    EntityManager em;
}
