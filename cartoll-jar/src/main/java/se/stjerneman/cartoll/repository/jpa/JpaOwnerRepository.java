package se.stjerneman.cartoll.repository.jpa;

import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import se.stjerneman.cartoll.domain.owner.Owner;
import se.stjerneman.cartoll.repository.OwnerRepository;

public class JpaOwnerRepository extends JpaAbstractRepository implements OwnerRepository {

    private static Logger log = Logger.getLogger(JpaOwnerRepository.class);

    @Override
    @Transactional
    public long save(Owner owner) {
        log.debug("About to save owner into db.");
        em.persist(owner);
        return owner.getId();
    }

    @Override
    @Transactional
    public void delete(long id) {
        log.debug("About to delete owner with id: " + id);
        em.remove(load(id));
    }

    @Override
    public Owner load(long id) {
        return em.find(Owner.class, id);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Owner> loadAll() {
        String queryString = "SELECT o FROM Owner o";
        Query query = em.createQuery(queryString);
        return query.getResultList();
    }

    @Override
    @Transactional
    public void deleteAll() {
        log.debug("About to delete all owners from db.");
        String queryString = "DELETE FROM Owner";
        Query query = em.createQuery(queryString);
        query.executeUpdate();
    }

}
