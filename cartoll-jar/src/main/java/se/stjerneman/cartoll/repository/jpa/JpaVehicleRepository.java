package se.stjerneman.cartoll.repository.jpa;

import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import se.stjerneman.cartoll.domain.vehicle.Vehicle;
import se.stjerneman.cartoll.repository.VehicleRepository;

public class JpaVehicleRepository extends JpaAbstractRepository implements VehicleRepository {

    private static Logger log = Logger.getLogger(JpaVehicleRepository.class);

    @Override
    @Transactional
    public long save(Vehicle vehicle) {
        log.debug("About to save vehicle into db.");
        em.persist(vehicle);
        return vehicle.getId();
    }

    @Override
    @Transactional
    public void delete(long id) {
        log.debug("About to delete vehicle with id: " + id);
        em.remove(load(id));
    }

    @Override
    public Vehicle load(long id) {
        return em.find(Vehicle.class, id);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Vehicle> loadAll() {
        String queryString = "SELECT v FROM Vehicle v";
        Query query = em.createQuery(queryString);
        return query.getResultList();
    }

    @Override
    @Transactional
    public void deleteAll() {
        log.debug("About to delete all vehicles from db.");
        String queryString = "DELETE FROM Vehicle";
        Query query = em.createQuery(queryString);
        query.executeUpdate();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Vehicle> loadAllOwnedBy(long owner_id) {
        String queryString = "SELECT v FROM Vehicle v WHERE owner_id = :owner_id";
        Query query = em.createQuery(queryString);
        query.setParameter("owner_id", owner_id);
        return query.getResultList();
    }

}
