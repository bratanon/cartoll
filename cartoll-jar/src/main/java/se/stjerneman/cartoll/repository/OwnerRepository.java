package se.stjerneman.cartoll.repository;

import java.util.List;

import se.stjerneman.cartoll.domain.owner.Owner;

/**
 * A repository class for handling owners in a repository.
 * 
 * @author Emil Stjerneman
 * 
 */
public interface OwnerRepository {

    /**
     * Permanently saves the given owner.
     * 
     * @param owner
     *            the owner to save.
     * @return the id of the saved owner.
     */
    public long save(Owner owner);

    /**
     * Permanently deletes a saved owner.
     * 
     * @param id
     *            the id of the owner to delete.
     */
    public void delete(long id);

    /**
     * Delete all stored owners.
     */
    public void deleteAll();

    /**
     * Loads a stored owner.
     * 
     * @param id
     *            the id of the owner to load.
     * @return an owner.
     */
    public Owner load(long id);

    /**
     * Loads all stored owners.
     * 
     * @return a list of all stored owners.
     */
    public List<Owner> loadAll();
}
