package se.stjerneman.cartoll.repository;

import java.util.List;

import se.stjerneman.cartoll.domain.vehicle.Vehicle;

/**
 * A repository class for handling vehicles in a repository.
 * 
 * @author Emil Stjerneman
 * 
 */
public interface VehicleRepository {

    /**
     * Permanently saves the given vehicle.
     * 
     * @param vehicle
     *            the vehicle to save.
     * @return the id of the saved vehicle.
     */
    public long save(Vehicle vehicle);

    /**
     * Permanently deletes a saved vehicle.
     * 
     * @param id
     *            the id of the vehicle to delete.
     */
    public void delete(long id);

    /**
     * Delete all stored vehicles.
     */
    public void deleteAll();

    /**
     * Loads a stored vehicle.
     * 
     * @param id
     *            the id of the vehicle to load.
     * @return a passage.
     */
    public Vehicle load(long id);

    /**
     * Loads all stored vehicles.
     * 
     * @return a list of all stored vehicles.
     */
    public List<Vehicle> loadAll();

    /**
     * Loads all stored vehicles owned by the provided owner id.
     * 
     * @param owner_id
     *            the owner to filer vehicles on.
     * @return a list of all stored vehicles owned by the provided owner id.
     */
    public List<Vehicle> loadAllOwnedBy(long owner_id);
}
