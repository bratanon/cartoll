package se.stjerneman.cartoll.repository.jpa;

import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import se.stjerneman.cartoll.domain.station.Station;
import se.stjerneman.cartoll.repository.StationRepository;

public class JpaStationRepository extends JpaAbstractRepository implements StationRepository {

    private static Logger log = Logger.getLogger(JpaStationRepository.class);

    @Override
    @Transactional
    public long save(Station station) {
        log.debug("About to save station into db.");
        em.persist(station);
        return station.getId();
    }

    @Override
    @Transactional
    public void delete(long id) {
        log.debug("About to delete station with id: " + id);
        em.remove(load(id));
    }

    @Override
    public Station load(long id) {
        return em.find(Station.class, id);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Station> loadAll() {
        String queryString = "SELECT s FROM Station s";
        Query query = em.createQuery(queryString);
        return query.getResultList();
    }

    @Override
    @Transactional
    public void deleteAll() {
        log.debug("About to delete all stations from db.");
        String queryString = "DELETE FROM Station";
        Query query = em.createQuery(queryString);
        query.executeUpdate();
    }
}
