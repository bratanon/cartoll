package se.stjerneman.cartoll.repository.mock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import se.stjerneman.cartoll.domain.owner.Owner;
import se.stjerneman.cartoll.repository.OwnerRepository;

public class MockOwnerRepository implements OwnerRepository {

    private static long ownerId = 1;

    private Map<Long, Owner> owners = new HashMap<>();

    @Override
    public long save(Owner owner) {
        owner.setId(ownerId++);
        owners.put(owner.getId(), owner);
        return owner.getId();
    }

    @Override
    public void delete(long id) {
        owners.remove(id);
    }

    @Override
    public Owner load(long id) {
        return owners.get(id);
    }

    @Override
    public List<Owner> loadAll() {
        return new ArrayList<>(owners.values());
    }

    @Override
    public void deleteAll() {
        owners.clear();
    }

}
