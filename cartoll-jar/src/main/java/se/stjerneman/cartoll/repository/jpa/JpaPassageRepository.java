package se.stjerneman.cartoll.repository.jpa;

import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import se.stjerneman.cartoll.domain.passage.Passage;
import se.stjerneman.cartoll.domain.statistic.Statistic;
import se.stjerneman.cartoll.repository.PassageRepository;

public class JpaPassageRepository extends JpaAbstractRepository implements PassageRepository {

    private static Logger log = Logger.getLogger(JpaPassageRepository.class);

    @Override
    @Transactional
    public long save(Passage passage) {
        log.debug("About to save passage into db.");
        em.persist(passage);
        return passage.getId();
    }

    @Override
    @Transactional
    public void delete(long id) {
        log.debug("About to delete passage with id: " + id);
        em.remove(load(id));
    }

    @Override
    public Passage load(long id) {
        return em.find(Passage.class, id);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Passage> loadAll() {
        String queryString = "SELECT p FROM Passage p";
        Query query = em.createQuery(queryString);
        return query.getResultList();
    }

    @Override
    @Transactional
    public void deleteAll() {
        log.debug("About to delete all passages from db.");
        String queryString = "DELETE FROM Passage";
        Query query = em.createQuery(queryString);
        query.executeUpdate();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Passage> loadAllByStationId(long station_id) {
        String queryString = "SELECT p FROM Passage p WHERE station_id = :station_id";
        Query query = em.createQuery(queryString);
        query.setParameter("station_id", station_id);
        return query.getResultList();
    }

    @Override
    public Statistic getStationStatistic(long station_id) {
        Query query = em.createQuery("SELECT new se.stjerneman.cartoll.domain.statistic.Statistic(SUM(p.cost), COUNT(p)) FROM Passage p WHERE station_id = :station_id");
        query.setParameter("station_id", station_id);
        return (Statistic) query.getSingleResult();
    }

    @Override
    public Statistic getVehicleStatistic(long vehicle_id) {
        Query query = em.createQuery("SELECT new se.stjerneman.cartoll.domain.statistic.Statistic(SUM(p.cost), COUNT(p)) FROM Passage p WHERE vehicle_id = :vehicle_id");
        query.setParameter("vehicle_id", vehicle_id);
        return (Statistic) query.getSingleResult();
    }
}
