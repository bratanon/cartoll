package se.stjerneman.cartoll.domain.vehicle;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import se.stjerneman.cartoll.domain.owner.Owner;

/**
 * An abstract class that handles vehicles.
 * 
 * @author Emil Stjerneman
 * 
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Vehicle {

    /**
     * This vehicles id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * This vehicles registration number.
     */
    private String regNo;

    /**
     * This vehicles owner.
     */
    @ManyToOne
    @JoinColumn(name = "owner_id")
    private Owner owner;

    /**
     * A map of fees for this vehicle.
     */
    @Transient
    protected Map<String, Integer> passageFees = new HashMap<>();

    /**
     * Empty constructor, used by JPA.
     */
    protected Vehicle () {

    }

    /**
     * Creates a basic vehicle.
     * 
     * @param regno
     *            this vehicles registration number.
     * @param owner
     *            this vehicles owner.
     */
    public Vehicle (String regno, Owner owner) {
        setRegNo(regno);
        setOwner(owner);
    }

    /**
     * Gets this vehicles id.
     * 
     * @return this vehicles id.
     */
    public long getId() {
        return id;
    }

    /**
     * Sets this vehicles id.
     * 
     * @param id
     *            this vehicles id.
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Gets this vehicles registration number.
     * 
     * @return this vehicles registration number.
     */
    public String getRegNo() {
        return regNo;
    }

    /**
     * Sets this vehicles registration number.
     * 
     * @param regNo
     *            this vehicles registration number.
     */
    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    /**
     * Gets this vehicles owner.
     * 
     * @return this vehicles owner.
     */
    public Owner getOwner() {
        return owner;
    }

    /**
     * Sets this vehicles owner.
     * 
     * @param owner
     *            this vehicles owner.
     */
    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    /**
     * Get the fee cost for this vehicle.
     * 
     * @param level
     *            the fee level to get.
     * @return a fee cost for this vehicle.
     */
    public abstract int getPassageFee(String level);

    /**
     * Builds a string with vehicle information.
     * 
     * @return a string with vehicle information.
     */
    public String getVehicleInformationString() {
        return getClass().getSimpleName() + " - " + getRegNo();
    }

    @Override
    public String toString() {
        return "Vehicle [id=" + id + ", regNo=" + regNo + ", owner=" + owner + ", passageFees=" + passageFees + "]";
    }
}
