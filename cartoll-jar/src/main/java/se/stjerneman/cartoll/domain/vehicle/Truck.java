package se.stjerneman.cartoll.domain.vehicle;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

import se.stjerneman.cartoll.domain.owner.Owner;

/**
 * A class that handles a truck vehicle.
 * 
 * @author Emil Stjerneman
 * 
 */
@Entity
@PrimaryKeyJoinColumn(name = "vehicle_id", referencedColumnName = "id")
public class Truck extends Vehicle {

    /**
     * This trucks weight in kilograms.
     */
    private float weight;

    /**
     * The extra fee for heavy trucks.
     */
    public static final int OVERWEIGHTFEE = 10;

    /**
     * The limit in kilograms when this truck is considered to have an over
     * weight.
     */
    public static final int WEIGHTLIMIT = 6000;

    /**
     * Empty constructor, used by JPA.
     */
    protected Truck () {
        super();
        setFees();
    }

    /**
     * Create a new truck.
     * 
     * @param regno
     *            this trucks registration number.
     * @param owner
     *            the owner of this truck.
     * @param weight
     *            this trucks weight in kilograms.
     */
    public Truck (String regno, Owner owner, float weight) {
        super(regno, owner);
        setWeight(weight);
        setFees();
    }

    /**
     * Gets this trucks weight.
     * 
     * @return this trucks weight in kilograms.
     */
    public float getWeight() {
        return weight;
    }

    /**
     * Sets this trucks weight.
     * 
     * @param weight
     *            this trucks weight in kilograms.
     */
    public void setWeight(float weight) {
        this.weight = weight;
    }

    /**
     * Calculates this trucks over weight.
     * 
     * @return this trucks potential over weight.
     */
    public double calculateOverWeight() {

        if (WEIGHTLIMIT >= getWeight()) {
            return 0;
        }

        return Math.abs(Math.ceil((WEIGHTLIMIT - getWeight()) / 1000));
    }

    /**
     * Overrides {@link Vehicle#getPassageFee(String)}
     * 
     * If the truck as an "over weight", the over weight fee is added to the
     * base fee.
     * 
     * @param level
     *            the fee level to get.
     * @return a fee cost for this vehicle.
     * 
     * @see Truck#OVERWEIGHTFEE
     * @see Truck#WEIGHTLIMIT
     */
    @Override
    public int getPassageFee(String level) {
        int fee = passageFees.get(level);

        if (calculateOverWeight() > 0) {
            fee += (calculateOverWeight() * OVERWEIGHTFEE);
        }

        return fee;
    }

    /**
     * Sets the fees for this truck.
     */
    private void setFees() {
        passageFees.put("free", 0);
        passageFees.put("low", 12);
        passageFees.put("medium", 20);
        passageFees.put("high", 25);
    }
}
