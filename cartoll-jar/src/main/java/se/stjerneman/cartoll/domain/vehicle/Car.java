package se.stjerneman.cartoll.domain.vehicle;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

import org.apache.log4j.Logger;

import se.stjerneman.cartoll.domain.owner.Owner;

/**
 * A class that handles a car vehicle.
 * 
 * @author Emil Stjerneman
 * 
 */
@Entity
@PrimaryKeyJoinColumn(name = "vehicle_id", referencedColumnName = "id")
public class Car extends Vehicle {

    private static Logger log = Logger.getLogger(Car.class);

    /**
     * Empty constructor, used by JPA.
     */
    protected Car () {
        super();
        setFees();
    }

    /**
     * Creates a car.
     * 
     * @see Vehicle#Vehicle(String, Owner)
     */
    public Car (String regno, Owner owner) {
        super(regno, owner);
        setFees();
    }

    @Override
    public int getPassageFee(String level) {
        return passageFees.get(level);
    }

    /**
     * Sets the fees for this car.
     */
    private void setFees() {
        passageFees.put("free", 0);
        passageFees.put("low", 8);
        passageFees.put("medium", 13);
        passageFees.put("high", 18);
    }

}
