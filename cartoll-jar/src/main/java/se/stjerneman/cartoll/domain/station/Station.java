package se.stjerneman.cartoll.domain.station;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * A station POJO class.
 * 
 * {@link http://goo.gl/PyTV5R}
 * 
 * @author Emil Stjerneman
 * 
 */
@Entity
public class Station {

    /**
     * This stations id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * This stations name.
     */
    private String name;

    /**
     * Empty constructor, used by JPA.
     */
    protected Station () {

    }

    /**
     * Creates a new station.
     * 
     * @param name
     *            this stations name.
     */
    public Station (String name) {
        setName(name);
    }

    /**
     * Gets this stations id.
     * 
     * @return this stations id.
     */
    public long getId() {
        return id;
    }

    /**
     * Sets this stations id.
     * 
     * @param id
     *            this stations id.
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Gets this stations name.
     * 
     * @return this stations name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets this stations name.
     * 
     * @param name
     *            this stations name.
     */
    public void setName(String name) {
        this.name = name;
    }
}
