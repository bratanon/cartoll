package se.stjerneman.cartoll.domain.vehicle;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

import se.stjerneman.cartoll.domain.owner.Owner;

/**
 * A class that handles a taxi vehicle.
 * 
 * @author Emil Stjerneman
 * 
 */
@Entity
@PrimaryKeyJoinColumn(name = "vehicle_id", referencedColumnName = "id")
public class Taxi extends Vehicle {

    /**
     * If the taxi is eco certified. Defaults to false.
     */
    private boolean ecoCertified = false;

    /**
     * The discount for eco certified taxis.
     */
    private static final int ECODISCOUNT = 5;

    /**
     * Empty constructor, used by JPA.
     */
    protected Taxi () {
        super();
        setFees();
    }

    /**
     * Create a new Taxi.
     * 
     * @see Vehicle#Vehicle(String, Owner)
     */
    public Taxi (String regno, Owner owner) {
        super(regno, owner);
        setFees();
    }

    /**
     * Create a new Taxi.
     * 
     * @param regno
     *            this taxis registration number.
     * @param owner
     *            the owner of this taxi.
     * @param isEco
     *            if this taxi is eco certified, true, otherwise false.
     */
    public Taxi (String regno, Owner owner, boolean isEco) {
        this(regno, owner);
        setEcoCertified(isEco);
    }

    /**
     * Gets this taxis eco certified status.
     * 
     * @return true if this taxi is eco certified, otherwise false.
     */
    public boolean isEcoCertified() {
        return ecoCertified;
    }

    /**
     * Sets this taxis eco certified status.
     * 
     * @param ecoCertified
     *            if this taxi is eco certified, true, otherwise false.
     */
    public void setEcoCertified(boolean ecoCertified) {
        this.ecoCertified = ecoCertified;
    }

    /**
     * Get the discount for eco certified taxis.
     * 
     * @return the discount for eco certified taxis.
     */
    public int getEcodiscount() {
        return ECODISCOUNT;
    }

    /**
     * Overrides {@link Vehicle#getPassageFee(String)}
     * 
     * If this taxi is eco certified, an eco discount is added to the base fee.
     * 
     * @param level
     *            the fee level to get.
     * @return a fee cost for this vehicle.
     * 
     * @see Taxi#ECODISCOUNT
     */
    @Override
    public int getPassageFee(String level) {
        int fee = passageFees.get(level);

        if (isEcoCertified() && fee > 0) {
            fee -= getEcodiscount();
        }

        return fee;
    }

    /**
     * Sets the fees for this taxi.
     */
    private void setFees() {
        passageFees.put("free", 0);
        passageFees.put("low", 15);
        passageFees.put("medium", 20);
        passageFees.put("high", 25);
    }

    @Override
    public String getVehicleInformationString() {
        String info = super.getVehicleInformationString();

        if (isEcoCertified()) {
            info += " (Miljöcertifierad)";
        }
        return info;
    }
}
