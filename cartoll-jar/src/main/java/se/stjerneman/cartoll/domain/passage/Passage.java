package se.stjerneman.cartoll.domain.passage;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.MutableDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import se.stjerneman.cartoll.domain.station.Station;
import se.stjerneman.cartoll.domain.vehicle.Vehicle;

/**
 * A passage object that handled all passage entities.
 * 
 * @author Emil Stjerneman
 * 
 */
@Entity
public class Passage {

    /**
     * This passage id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * The station this passage is made for.
     */
    @ManyToOne
    @JoinColumn(name = "station_id")
    private Station station;

    /**
     * Passage time.
     */
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime dateTime;

    /**
     * The vehicle responsible for this passage.
     */
    @ManyToOne
    @JoinColumn(name = "vehicle_id")
    private Vehicle vehicle;

    /**
     * The total cost for this passage.
     */
    private int cost;

    /**
     * Empty constructor, used by JPA.
     */
    protected Passage () {

    }

    /**
     * Create a passage.
     * 
     * @param station
     *            the station this passage is made on.
     * @param vehicle
     *            the vehicle that made this passage.
     * @param datetime
     *            the time this passage where made.
     */
    public Passage (Station station, Vehicle vehicle, DateTime datetime) {
        setStation(station);
        setVehicle(vehicle);
        setDateTime(datetime);
        calculateCost();
    }

    /**
     * Gets this passages id.
     * 
     * @return this passages id.
     */
    public long getId() {
        return id;
    }

    /**
     * Sets this passages id.
     * 
     * @param id
     *            this passages id.
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Gets this passages station.
     * 
     * @return this passages station.
     */
    public Station getStation() {
        return station;
    }

    /**
     * Sets this passage station.
     * 
     * @param station
     *            this passage station.
     */
    public void setStation(Station station) {
        this.station = station;
    }

    /**
     * Gets this passages time.
     * 
     * @return this passages time.
     */
    public DateTime getDateTime() {
        return dateTime;
    }

    /**
     * Gets this passages time formated in a nice way.
     * 
     * @return this passages time formated in a nice way.
     */
    public String getFormatedDateTime() {
        DateTimeFormatter fmt = DateTimeFormat.forPattern("y-MM-dd HH:mm");
        return fmt.print(dateTime);
    }

    /**
     * Sets this passages time.
     * 
     * @param datetime
     *            this passages time.
     */
    public void setDateTime(DateTime datetime) {
        this.dateTime = datetime;
    }

    /**
     * Gets this passages vehicle.
     * 
     * @return this passages vehicle.
     */
    public Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Sets this passages vehicle.
     * 
     * @param vehicle
     *            this passages vehicle.
     */
    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    /**
     * Gets this passages cost.
     * 
     * @return this passages cost.
     */
    public int getCost() {
        return this.cost;
    }

    /**
     * Sets this passages cost.
     * 
     * @param cost
     *            this passages cost.
     */
    public void setCost(int cost) {
        this.cost = cost;
    }

    /**
     * Calculate the total cost of this passage.
     */
    private void calculateCost() {
        TimeUtil timeUtil = new TimeUtil(dateTime);

        if (timeUtil.isEqualOrAfter(18, 30)) {
            setCost(vehicle.getPassageFee("free"));
        }
        else if (timeUtil.isEqualOrAfter(18, 0)) {
            setCost(vehicle.getPassageFee("low"));
        }
        else if (timeUtil.isEqualOrAfter(17, 0)) {
            setCost(vehicle.getPassageFee("medium"));
        }
        else if (timeUtil.isEqualOrAfter(15, 30)) {
            setCost(vehicle.getPassageFee("high"));
        }
        else if (timeUtil.isEqualOrAfter(15, 0)) {
            setCost(vehicle.getPassageFee("medium"));
        }
        else if (timeUtil.isEqualOrAfter(8, 30)) {
            setCost(vehicle.getPassageFee("low"));
        }
        else if (timeUtil.isEqualOrAfter(8, 0)) {
            setCost(vehicle.getPassageFee("medium"));
        }
        else if (timeUtil.isEqualOrAfter(7, 0)) {
            setCost(vehicle.getPassageFee("high"));
        }
        else if (timeUtil.isEqualOrAfter(6, 30)) {
            setCost(vehicle.getPassageFee("medium"));
        }
        else if (timeUtil.isEqualOrAfter(6, 0)) {
            setCost(vehicle.getPassageFee("low"));
        }
    }

    /**
     * Class to help passages to calculate time intervals.
     * 
     * @author Emil Stjerneman
     * 
     */
    protected static class TimeUtil {

        private MutableDateTime startPointDateTime = new MutableDateTime();

        private DateTime passageDateTime;

        public TimeUtil (DateTime dateTime) {
            passageDateTime = dateTime;
            int year = dateTime.getYear();
            int mount = dateTime.getMonthOfYear();
            int day = dateTime.getDayOfMonth();
            startPointDateTime.setYear(year);
            startPointDateTime.setMonthOfYear(mount);
            startPointDateTime.setDayOfMonth(day);
        }

        public boolean isEqualOrAfter(int hour, int minute) {
            startPointDateTime.setHourOfDay(hour);
            startPointDateTime.setMinuteOfHour(minute);
            // We don't care about seconds and millis so reset them to zero.
            startPointDateTime.setSecondOfMinute(0);
            startPointDateTime.setMillisOfSecond(0);
            return passageDateTime.isAfter(startPointDateTime) || passageDateTime.isEqual(startPointDateTime);
        }
    }

    @Override
    public String toString() {
        return "Passage [id=" + id + ", station=" + station.getId() + ", dateTime=" + dateTime + ", vehicle=" + vehicle.getId() + ", cost=" + cost + "]";
    }

}
