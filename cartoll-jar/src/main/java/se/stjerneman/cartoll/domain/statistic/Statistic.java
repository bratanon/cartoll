package se.stjerneman.cartoll.domain.statistic;

/**
 * Handle statistic data.
 * 
 * @author Emil Stjerneman
 * 
 */
// TODO: DOC THIS.
public class Statistic {

    private long income;

    private long passageQuantity;

    public Statistic (Long income, long passageQuantity) {
        this.income = (income == null) ? 0 : income;
        this.passageQuantity = passageQuantity;
    }

    public long getIncome() {
        return income;
    }

    public void setIncome(long income) {
        this.income = income;
    }

    public long getPassageQuantity() {
        return passageQuantity;
    }

    public void setPassageQuantity(long passageQuantity) {
        this.passageQuantity = passageQuantity;
    }
}
