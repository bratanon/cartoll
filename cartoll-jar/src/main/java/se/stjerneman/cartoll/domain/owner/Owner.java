package se.stjerneman.cartoll.domain.owner;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import se.stjerneman.cartoll.domain.vehicle.Vehicle;

/**
 * An owner POJO class.
 * 
 * @author Emil Stjerneman
 * 
 */
@Entity
public class Owner {

    /**
     * This owners id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * This owners first name.
     */
    private String firstName;

    /**
     * This owners last name.
     */
    private String lastName;

    /**
     * This owners address.
     */
    private String address;

    /**
     * This owners zip code.
     */
    private String zipCode;

    /**
     * This owners city.
     */
    private String city;

    /**
     * This owners vehicles.
     */
    @OneToMany(mappedBy = "owner", cascade = CascadeType.REMOVE)
    private Set<Vehicle> vehicleList = new HashSet<Vehicle>();

    /**
     * Empty constructor, used by JPA.
     */
    protected Owner () {

    }

    /**
     * Creates an owner.
     * 
     * @param firstName
     *            this owners first name.
     * @param lastName
     *            this owners last name.
     */
    public Owner (String firstName, String lastName) {
        setFirstName(firstName);
        setLastName(lastName);
    }

    /**
     * Creates an owner.
     * 
     * @param firstName
     *            this owners first name.
     * @param lastName
     *            this owners last name.
     * @param address
     *            this owners address.
     * @param zipCode
     *            this owners zip code.
     * @param city
     *            this owners city.
     */
    public Owner (String firstName, String lastName, String address, String zipCode, String city) {
        this(firstName, lastName);
        setAddress(address);
        setZipCode(zipCode);
        setCity(city);
    }

    /**
     * Gets this owners id.
     * 
     * @return this owners id.
     */
    public long getId() {
        return id;
    }

    /**
     * Sets this owners id.
     * 
     * @param id
     *            this owners id.
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Gets this owners first name.
     * 
     * @return this owners first name.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets this owners first name.
     * 
     * @param firstName
     *            this owners first name.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets this owners last name.
     * 
     * @return this owners last name.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets this owners last name.
     * 
     * @param lastName
     *            this owners last name.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets this owners address.
     * 
     * @return this owners address.
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets this owners address.
     * 
     * @param address
     *            this owners address.
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Gets this owners zip code.
     * 
     * @return this owners zip code.
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     * Sets this owners zip code.
     * 
     * @param zipCode
     *            this owners zip code.
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    /**
     * Get this owners city.
     * 
     * @return this owners city.
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets this owners city.
     * 
     * @param city
     *            this owners city.
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Gets all vehicles owned by this owner.
     * 
     * @return all vehicles owned by this owner.
     */
    public Set<Vehicle> getVehicleList() {
        return vehicleList;
    }

    /**
     * Sets the set of all vehicles owned by this owner.
     * 
     * @param vehicles
     *            a set of all vehicles owned by this owner.
     */
    public void setVehicles(Set<Vehicle> vehicles) {
        vehicleList = vehicles;
    }

    /**
     * Adds a vehicle to the set of vehicles owned by this owner.
     * 
     * @param vehicle
     */
    public void addVehicleToOwner(Vehicle vehicle) {
        vehicleList.add(vehicle);
    }

    /**
     * Gets this owners full name.
     * 
     * @return this owners full name.
     */
    public String getFullName() {
        return firstName + " " + lastName;
    }

    @Override
    public String toString() {
        return "Owner [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", address=" + address + ", zipCode=" + zipCode + ", city=" + city + "]";
    }
}
